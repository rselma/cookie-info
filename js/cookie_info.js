(function ($) {
  Drupal.behaviors.cookie_info_popup = {
    attach: function(context, settings) {
      $('body').not('.sliding-popup-processed').addClass('sliding-popup-processed').each(function() {
        try {
          var enabled = Drupal.settings.cookie_info.popup_enabled;
          if(!enabled) {
            return;
          }
          if (!Drupal.cookie_info.cookiesEnabled()) {
            return;
          } 
          var status = Drupal.cookie_info.getCurrentStatus();
          var clicking_confirms = Drupal.settings.cookie_info.popup_clicking_confirmation;
          var agreed_enabled = Drupal.settings.cookie_info.popup_agreed_enabled;
          var popup_hide_agreed = Drupal.settings.cookie_info.popup_hide_agreed;
          if (status == 0) {
            var next_status = 1;
            if (clicking_confirms) {
              $('a, input[type=submit]').bind('click.cookie_info', function(){
                if(!agreed_enabled) {
                  Drupal.cookie_info.setStatus(1);
                  next_status = 2;
                }
                Drupal.cookie_info.changeStatus(next_status);
              });
            }

            $('.agree-button').click(function(){
              if(!agreed_enabled) {
                Drupal.cookie_info.setStatus(1);
                next_status = 2;
              }
              Drupal.cookie_info.changeStatus(next_status);
            });

            Drupal.cookie_info.createPopup(Drupal.settings.cookie_info.popup_html_info);
          } else if(status == 1) {
            Drupal.cookie_info.createPopup(Drupal.settings.cookie_info.popup_html_agreed);
            if (popup_hide_agreed) {
              $('a, input[type=submit]').bind('click.cookie_info_hideagreed', function(){
                Drupal.cookie_info.changeStatus(2);
              });
            }

          } else {
            return;
          }
        }
        catch(e) {
          return;
        }
      });
    }
  }

  Drupal.cookie_info = {};

  Drupal.cookie_info.createPopup = function(html) {
    var popup = $(html)
      .attr({"id": "sliding-popup"})
      .height(Drupal.settings.cookie_info.popup_height)
      .width(Drupal.settings.cookie_info.popup_width)
      .hide();
    if(Drupal.settings.cookie_info.popup_position) {
      popup.prependTo("body");
      var height = popup.height();
      popup.show()
        .attr({"class": "sliding-popup-top"})
        .css({"top": -1 * height})
        .animate({top: 0}, Drupal.settings.cookie_info.popup_delay);
    } else {
      popup.appendTo("body");
      height = popup.height();
      popup.show()
        .attr({"class": "sliding-popup-bottom"})
        .css({"bottom": -1 * height})
        .animate({bottom: 0}, Drupal.settings.cookie_info.popup_delay)
    }
    Drupal.cookie_info.attachEvents();
  }

  Drupal.cookie_info.attachEvents = function() {
	var clicking_confirms = Drupal.settings.cookie_info.popup_clicking_confirmation;
    var agreed_enabled = Drupal.settings.cookie_info.popup_agreed_enabled;
    $('.find-more-button').click(function(){
      if (Drupal.settings.cookie_info.popup_link_new_window) {
        window.open(Drupal.settings.cookie_info.popup_link);
      }
      else{
        window.location.href = Drupal.settings.cookie_info.popup_link;
      }
    });
    $('.agree-button').click(function(){
      var next_status = 1;
      if(!agreed_enabled) {
        Drupal.cookie_info.setStatus(1);
        next_status = 2;
      }
      if (clicking_confirms) {
        $('a, input[type=submit]').unbind('click.cookie_info');
      }
      Drupal.cookie_info.changeStatus(next_status);
    });
    $('.hide-popup-button').click(function(){
      Drupal.cookie_info.changeStatus(2);
    });
  }

  Drupal.cookie_info.getCurrentStatus = function() {
	name = 'cookie-agreed';
	value = Drupal.cookie_info.getCookie(name);
	return value;
  }

  Drupal.cookie_info.changeStatus = function(value) {
    var status = Drupal.cookie_info.getCurrentStatus();
    if (status == value) return;
    if(Drupal.settings.cookie_info.popup_position) {
      $(".sliding-popup-top").animate({top: $("#sliding-popup").height() * -1}, Drupal.settings.cookie_info.popup_delay, function () {
        if(status == 0) {
          $("#sliding-popup").html(Drupal.settings.cookie_info.popup_html_agreed).animate({top: 0}, Drupal.settings.cookie_info.popup_delay);
          Drupal.cookie_info.attachEvents();
        }
        if(status == 1) {
          $("#sliding-popup").remove();
        }
      })
    } else {
      $(".sliding-popup-bottom").animate({bottom: $("#sliding-popup").height() * -1}, Drupal.settings.cookie_info.popup_delay, function () {
        if(status == 0) {
          $("#sliding-popup").html(Drupal.settings.cookie_info.popup_html_agreed).animate({bottom: 0}, Drupal.settings.cookie_info.popup_delay)
          Drupal.cookie_info.attachEvents();
        }
        if(status == 1) {
          $("#sliding-popup").remove();
        }
      ;})
    }
    Drupal.cookie_info.setStatus(value);
  }

  Drupal.cookie_info.setStatus = function(status) {
    var date = new Date();
    date.setDate(date.getDate() + 100);
    var cookie = "cookie-agreed=" + status + ";expires=" + date.toUTCString() + ";path=" + Drupal.settings.basePath;
    if(Drupal.settings.cookie_info.domain) {
      cookie += ";domain="+Drupal.settings.cookie_info.domain;
    }
    document.cookie = cookie;
  }

  Drupal.cookie_info.hasAgreed = function() {
    var status = Drupal.cookie_info.getCurrentStatus();
    if(status == 1 || status == 2) {
      return true;
    }
    return false;
  }


  /**
   * Verbatim copy of Drupal.comment.getCookie().
   */
  Drupal.cookie_info.getCookie = function(name) {
    var search = name + '=';
    var returnValue = '';

    if (document.cookie.length > 0) {
      offset = document.cookie.indexOf(search);
      if (offset != -1) {
        offset += search.length;
        var end = document.cookie.indexOf(';', offset);
        if (end == -1) {
          end = document.cookie.length;
        }
        returnValue = decodeURIComponent(document.cookie.substring(offset, end).replace(/\+/g, '%20'));
      }
    }

    return returnValue;
  };
  
  Drupal.cookie_info.cookiesEnabled = function() {
    var cookieEnabled = (navigator.cookieEnabled) ? true : false;
      if (typeof navigator.cookieEnabled == "undefined" && !cookieEnabled) { 
        document.cookie="testcookie";
        cookieEnabled = (document.cookie.indexOf("testcookie") != -1) ? true : false;
      }
    return (cookieEnabled);
  }
  
})(jQuery);